package io.github.winx64.smartclans.util;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;

public final class Util {

	public static final List<String> EMPTY_LIST = Collections.emptyList();

	private Util() {}

	public static void close(Closeable closeable) {
		try {
			closeable.close();
		} catch (Exception ignored) {}
	}

	public static void close(Connection connection) {
		try {
			connection.close();
		} catch (Exception ignored) {}
	}

	public static void close(Statement statement) {
		try {
			statement.close();
		} catch (Exception ignored) {}
	}

	public static void close(ResultSet result) {
		try {
			result.close();
		} catch (Exception ignored) {}
	}
}
