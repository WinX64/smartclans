package io.github.winx64.smartclans.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import io.github.winx64.smartclans.SmartClans;
import io.github.winx64.smartclans.clan.Clan;
import io.github.winx64.smartclans.configuration.ClanConfiguration;
import io.github.winx64.smartclans.util.Util;

public final class DatabaseManager {

	private final SmartClans plugin;
	private final ClanConfiguration config;

	private HikariDataSource connectionPool;
	private HikariConfig connectionPoolConfig;

	public DatabaseManager(SmartClans plugin) {
		this.plugin = plugin;
		this.config = plugin.getClanConfiguration();
	}

	public boolean initialize() {
		try {
			plugin.log("[DB] Initializing database pool configuration...");
			loadPoolConfiguration();

			plugin.log("[DB] Establishing database connection...");
			initializeConnection();

			plugin.log("[DB] Database connection established with success!");
			return true;
		} catch (Exception e) {
			plugin.log(Level.SEVERE, e,
					"[DB] An error occurred while trying to initialize the database and its components! Details below:");
			return false;
		}
	}

	public void dispose() {
		try {
			if (connectionPool == null) {
				return;
			}

			this.connectionPool.close();
			plugin.log("[DB] Database connection closed with success!");
		} catch (Exception e) {
			plugin.log(Level.SEVERE, e,
					"[DB] An error occurred while trying to dispose of the database resources! Details below:");
		}
	}

	private void loadPoolConfiguration() {
		this.connectionPoolConfig = new HikariConfig();
		this.connectionPoolConfig.setJdbcUrl(String.format("jdbc:%s://%s:%d/%s", config.getSQLDriver(),
				config.getSQLAddress(), config.getSQLPort(), config.getSQLDatabase()));
		this.connectionPoolConfig.addDataSourceProperty("user", config.getSQLUsername());
		this.connectionPoolConfig.addDataSourceProperty("password", config.getSQLPassword());
		this.connectionPoolConfig.setPoolName("Server Pool");
		this.connectionPoolConfig.setMinimumIdle(config.getPoolMinConnections());
		this.connectionPoolConfig.setMaximumPoolSize(config.getPoolMaxConnections());
		this.connectionPoolConfig.setIdleTimeout(config.getPoolIdleTimeout());
		this.connectionPoolConfig
				.setThreadFactory(new ThreadFactoryBuilder().setNameFormat("Connection Thread #%d").build());
	}

	private void initializeConnection() {
		this.connectionPool = new HikariDataSource(connectionPoolConfig);
		System.out.println(connectionPool);
	}

	private synchronized Connection getConnection() {
		try {
			return connectionPool.getConnection();
		} catch (Exception e) {
			return null;
		}
	}

	public Executor executor() {
		return new Executor(getConnection());
	}

	public final class Executor implements AutoCloseable {

		private final Connection connection;

		private Executor(Connection connection) {
			this.connection = connection;
		}

		public Search<Clan> loadClan(int id) {
			PreparedStatement selectStatement = null;
			ResultSet result = null;
			try {
				selectStatement = connection.prepareStatement("SELECT * FROM `clans` WHERE `id`=?");
				selectStatement.setInt(1, id);
				result = selectStatement.executeQuery();

				if (!result.first()) {
					return Search.notFound();
				}

				return Search.foundOne(loadClan(result));
			} catch (Exception e) {
				return Search.error();
			} finally {
				Util.close(result);
				Util.close(selectStatement);
			}
		}

		public Search<Clan> loadClan(String name) {
			PreparedStatement selectStatement = null;
			ResultSet result = null;
			try {
				selectStatement = connection.prepareStatement("SELECT * FROM `clans` WHERE `name`=?");
				selectStatement.setString(1, name);
				result = selectStatement.executeQuery();

				if (!result.first()) {
					return Search.notFound();
				}

				return Search.foundOne(loadClan(result));
			} catch (Exception e) {
				return Search.error();
			} finally {
				Util.close(result);
				Util.close(selectStatement);
			}
		}

		private Clan loadClan(ResultSet result) throws SQLException {
			int id = result.getInt("id");
			String name = result.getString("name");

			return new Clan(id, name);
		}

		@Override
		public void close() {
			Util.close(connection);
		}
	}
}
