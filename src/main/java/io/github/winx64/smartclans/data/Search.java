package io.github.winx64.smartclans.data;

public final class Search<T> {

	private final Result result;
	private final T data;

	private Search(Result result, T data) {
		this.result = result;
		this.data = data;
	}

	public Result getResult() {
		return result;
	}

	public T getData() {
		return data;
	}

	public static <T> Search<T> foundOne(T data) {
		return new Search<T>(Result.FOUND_ONE, data);
	}

	public static <T> Search<T> foundMultiple(T data) {
		return new Search<T>(Result.FOUND_MULTIPLE, data);
	}

	public static <T> Search<T> notFound() {
		return new Search<T>(Result.NOT_FOUND, null);
	}

	public static <T> Search<T> error() {
		return new Search<T>(Result.ERROR, null);
	}

	public static enum Result {
		FOUND_ONE, FOUND_MULTIPLE, NOT_FOUND, ERROR
	}
}
