package io.github.winx64.smartclans.configuration;

import java.io.File;
import java.util.logging.Level;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import io.github.winx64.smartclans.SmartClans;

public final class ClanConfiguration {

	private static final String CONFIG_FILE_NAME = "config.yml";
	private static final String OLD_CONFIG_FILE_NAME = "config-old-%d.yml";
	private static final String CONFIG_VERSION_KEY = "config-version";
	private static final int CONFIG_VERSION = 1;

	private final SmartClans plugin;
	private final File configFile;
	private FileConfiguration config;

	private String sqlDriver;
	private String sqlAddress;
	private int sqlPort;
	private String sqlUsername;
	private String sqlPassword;
	private String sqlDatabase;

	private int poolMaxConnections;
	private int poolMinConnections;
	private int poolIdleTimeout;

	public ClanConfiguration(SmartClans plugin) {
		this.plugin = plugin;
		this.configFile = new File(plugin.getDataFolder(), CONFIG_FILE_NAME);

		this.sqlDriver = "mysql";
		this.sqlAddress = "locahost";
		this.sqlPort = 3306;
		this.sqlUsername = "root";
		this.sqlPassword = "password";
		this.sqlDatabase = "smart_clans";

		this.poolMaxConnections = 3;
		this.poolMinConnections = 1;
		this.poolIdleTimeout = 30000;
	}

	public String getSQLDriver() {
		return sqlDriver;
	}

	public String getSQLAddress() {
		return sqlAddress;
	}

	public int getSQLPort() {
		return sqlPort;
	}

	public String getSQLUsername() {
		return sqlUsername;
	}

	public String getSQLPassword() {
		return sqlPassword;
	}

	public String getSQLDatabase() {
		return sqlDatabase;
	}

	public int getPoolMaxConnections() {
		return poolMaxConnections;
	}

	public int getPoolMinConnections() {
		return poolMinConnections;
	}

	public int getPoolIdleTimeout() {
		return poolIdleTimeout;
	}

	public boolean loadConfiguration() {
		try {
			if (!configFile.exists()) {
				plugin.log(Level.INFO, "[Config] Config file not found. Creating a new one...");
				plugin.saveResource(CONFIG_FILE_NAME, true);
			}

			this.config = YamlConfiguration.loadConfiguration(configFile);
			if (config.getKeys(false).size() == 0) {
				plugin.log(Level.SEVERE, "[Config] Empty configuration! Did any error happen while parsing it?");
				return false;
			}

			if (!ensureCorrectVersion(true)) {
				plugin.log(Level.SEVERE, "[Config] Could not load the correct version of the configuration!",
						CONFIG_VERSION);
				return false;
			}

			loadDatabaseConnectionConfig(config.getConfigurationSection("database-connection"));
			loadConnectionPoolConfig(config.getConfigurationSection("connection-pool"));

			plugin.log(Level.INFO, "[Config] Configuration loaded with success!");
			return true;
		} catch (Exception e) {
			plugin.log(Level.SEVERE, e,
					"[Config] An error occurred while trying to load the configuration! Details below:");
			return false;
		}
	}

	private boolean ensureCorrectVersion(boolean saveAndRetry) {
		int currentVersion = config.getInt(CONFIG_VERSION_KEY, -1);
		if (currentVersion == -1 && saveAndRetry) {
			plugin.log(Level.WARNING, "[Config] The configuration version is missing. Did you erase it by accident?");
			plugin.log(Level.INFO, "[Config] Creating an up to date one...");
			plugin.saveResource(CONFIG_FILE_NAME, true);
			this.config = YamlConfiguration.loadConfiguration(configFile);
			return ensureCorrectVersion(false);
		}

		if (currentVersion != CONFIG_VERSION) {
			if (saveAndRetry) {
				plugin.log(Level.WARNING, "[Config] Outdated configuration detected. Preparing to create a new one...");
				if (!moveOldConfiguration()) {
					plugin.log(Level.WARNING, "[Config] Failed to move old configuration. Overwritting it...");
				}
				plugin.saveResource(CONFIG_FILE_NAME, true);
				this.config = YamlConfiguration.loadConfiguration(configFile);
				return ensureCorrectVersion(false);
			} else {
				return false;
			}
		}

		return true;
	}

	private boolean moveOldConfiguration() {
		try {
			String newFileName = String.format(OLD_CONFIG_FILE_NAME, System.currentTimeMillis());
			File newFile = new File(plugin.getDataFolder(), newFileName);
			plugin.log(Level.INFO, "[Config] The old %s is now \"%s\"", CONFIG_FILE_NAME, newFileName);
			configFile.renameTo(newFile);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void loadDatabaseConnectionConfig(ConfigurationSection databaseConfig) {
		if (databaseConfig == null) {
			plugin.log(Level.WARNING, "[Config] The 'database-connection' section is empty! Using default values.");
			return;
		}

		if (databaseConfig.contains("driver")) {
			this.sqlDriver = databaseConfig.getString("driver");
		} else {
			plugin.log("[Config] The 'driver' option is not set! Using default value of '%s'", sqlDriver);
		}
		if (databaseConfig.contains("address")) {
			this.sqlAddress = databaseConfig.getString("address");
		} else {
			plugin.log("[Config] The 'address' option is not set! Using default value of '%s'", sqlAddress);
		}
		if (databaseConfig.contains("port")) {
			this.sqlPort = databaseConfig.getInt("port");
		} else {
			plugin.log("[Config] The 'port' option is not set! Using default value of '%d'", sqlPort);
		}
		if (databaseConfig.contains("username")) {
			this.sqlUsername = databaseConfig.getString("username");
		} else {
			plugin.log("[Config] The 'username' option is not set! Using default value of '%s'", sqlUsername);
		}
		if (databaseConfig.contains("password")) {
			this.sqlPassword = databaseConfig.getString("password");
		} else {
			plugin.log("[Config] The 'password' option is not set! Using default value of '%s'", sqlPassword);
		}
		if (databaseConfig.contains("database")) {
			this.sqlDatabase = databaseConfig.getString("database");
		} else {
			plugin.log("[Config] The 'database' option is not set! Using default value of '%s'", sqlDatabase);
		}
	}

	private void loadConnectionPoolConfig(ConfigurationSection poolConfig) {
		if (poolConfig == null) {
			plugin.log(Level.WARNING, "[Config] The 'connection-pool' section is empty! Using default values.");
			return;
		}

		if (poolConfig.contains("max-connections")) {
			this.poolMaxConnections = poolConfig.getInt("max-connections");
		} else {
			plugin.log("[Config] The 'max-connections' option is not set! Using default value of '%d'",
					poolMaxConnections);
		}
		if (poolConfig.contains("min-connections")) {
			this.poolMinConnections = poolConfig.getInt("min-connections");
		} else {
			plugin.log("[Config] The 'min-connections' option is not set! Using default value of '%d'",
					poolMinConnections);
		}
		if (poolConfig.contains("idle-timeout")) {
			this.poolIdleTimeout = poolConfig.getInt("idle-timeout");
		} else {
			plugin.log("[Config] The 'idle-timeout' option is not set! Using default value of '%d'", poolIdleTimeout);
		}
	}
}
