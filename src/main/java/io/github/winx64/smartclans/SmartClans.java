package io.github.winx64.smartclans;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.java.JavaPlugin;

import io.github.winx64.smartclans.command.parent.CommandClan;
import io.github.winx64.smartclans.configuration.ClanConfiguration;
import io.github.winx64.smartclans.data.DatabaseManager;

public final class SmartClans extends JavaPlugin {

	private final Logger logger;

	private final ClanConfiguration clanConfig;
	private final DatabaseManager database;

	public SmartClans() {
		this.logger = getLogger();

		this.clanConfig = new ClanConfiguration(this);
		this.database = new DatabaseManager(this);
	}

	@Override
	public void onEnable() {

		log("Loading configuration...");
		if (!clanConfig.loadConfiguration()) {
			halt("Configuration load failure");
			return;
		}

		log("Registering commands...");
		if (!registerCommands()) {
			halt("Command registration failure");
			return;
		}

		log("Initializing database...");
		if (!database.initialize()) {
			halt("Database initialization failure");
			return;
		}
	}

	@Override
	public void onDisable() {
		this.database.dispose();
	}

	private void halt(String reason) {
		log(Level.SEVERE, "The plugin will now be disabled. Reason: %s", reason);
		Bukkit.getPluginManager().disablePlugin(this);
	}

	private boolean registerCommands() {
		try {
			registerCommand("clan", new CommandClan(this));
			log("[Commands] Commands registered with success!");
			return true;
		} catch (Exception e) {
			log(Level.SEVERE, e, "[Commands] An error occurred while trying to register the commands. Details below:");
			return false;
		}
	}

	private void registerCommand(String commandName, CommandExecutor executor) {
		PluginCommand command = getCommand(commandName);
		command.setExecutor(executor);
		if (executor instanceof TabCompleter) {
			command.setTabCompleter((TabCompleter) executor);
		}
	}

	public ClanConfiguration getClanConfiguration() {
		return clanConfig;
	}

	public DatabaseManager getDatabaseManager() {
		return database;
	}

	public void log(String format, Object... args) {
		log(Level.INFO, null, String.format(format, args));
	}

	public void log(String message) {
		log(Level.INFO, null, message);
	}

	public void log(Level level, String format, Object... args) {
		log(level, null, String.format(format, args));
	}

	public void log(Level level, Exception e, String format, Object... args) {
		log(level, e, String.format(format, args));
	}

	public void log(Level level, String message) {
		log(level, null, message);
	}

	public void log(Level level, Exception e, String message) {
		logger.log(level, message, e);
	}
}
