package io.github.winx64.smartclans.player;

import org.bukkit.entity.Player;

import io.github.winx64.smartclans.clan.Clan;

public final class ClanPlayer {

	private final Player player;

	private Clan clan;

	public ClanPlayer(Player player) {
		this.player = player;
		this.clan = null;
	}

	public Player getPlayer() {
		return player;
	}

	public Clan getClan() {
		return clan;
	}

	public void setClan(Clan clan) {
		this.clan = clan;
	}
}
