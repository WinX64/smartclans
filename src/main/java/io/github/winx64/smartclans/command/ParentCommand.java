package io.github.winx64.smartclans.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import io.github.winx64.smartclans.SmartClans;
import io.github.winx64.smartclans.util.Util;

public abstract class ParentCommand extends SubCommand {

	protected final Map<String, SubCommand> subCommands;
	protected final String defaultSubCommand;

	public ParentCommand(SmartClans plugin, ParentCommand parent, String name, String permission,
			String defaultSubCommand) {
		super(plugin, parent, name, permission);

		this.subCommands = new HashMap<String, SubCommand>();
		this.defaultSubCommand = defaultSubCommand;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		String subCommandName = args.length < 1 ? defaultSubCommand : args[0].toLowerCase();
		SubCommand subCommand = subCommands.get(subCommandName);

		if (subCommand == null) {
			if (subCommandName == null) {
				sender.sendMessage(ChatColor.RED + "You must specify a sub-command!");
			} else {
				sender.sendMessage(ChatColor.RED + "The sub-command '" + subCommandName + "' doesn't exist!");
			}
			return true;
		}

		if (!sender.hasPermission(subCommand.getPermission())) {
			sender.sendMessage(
					ChatColor.RED + "You don't have permission to use the sub-command '" + subCommandName + "'!");
			return true;
		}

		String[] subArgs = args.length < 1 ? args : Arrays.copyOfRange(args, 1, args.length);
		return subCommand.onCommand(sender, command, alias, subArgs);
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if (args.length == 2) {
			String subCommandName = args[0].toLowerCase();
			SubCommand subCommand = this.subCommands.get(subCommandName);

			if (subCommand != null && sender.hasPermission(subCommand.getPermission())) {
				String[] subArgs = Arrays.copyOfRange(args, 1, args.length);
				return subCommand.onTabComplete(sender, command, alias, subArgs);
			}
		} else if (args.length == 1) {
			String match = args[0].toLowerCase();
			List<String> matches = new ArrayList<String>();
			for (Entry<String, SubCommand> entry : this.subCommands.entrySet()) {
				if (entry.getKey().startsWith(match) && sender.hasPermission(entry.getValue().getPermission())) {
					matches.add(entry.getKey());
				}
			}
			return matches;
		}
		return Util.EMPTY_LIST;
	}

	protected final void registerSubCommand(SubCommand subCommand) {
		this.subCommands.put(subCommand.getName(), subCommand);
	}
}
