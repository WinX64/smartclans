package io.github.winx64.smartclans.command.sub;

import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import io.github.winx64.smartclans.SmartClans;
import io.github.winx64.smartclans.command.ParentCommand;
import io.github.winx64.smartclans.command.SubCommand;

public final class SubCommandClanCreate extends SubCommand {

	private static final String NAME = "create";

	public SubCommandClanCreate(SmartClans plugin, ParentCommand parent) {
		super(plugin, parent, NAME, parent.getPermission() + "." + NAME);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		sender.sendMessage("SoonTM - " + Arrays.toString(args));
		return true;
	}
}
