package io.github.winx64.smartclans.command.parent;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.winx64.smartclans.SmartClans;
import io.github.winx64.smartclans.command.ParentCommand;
import io.github.winx64.smartclans.command.sub.SubCommandClanCreate;
import io.github.winx64.smartclans.command.sub.SubCommandClanMenu;
import io.github.winx64.smartclans.util.Util;

public final class CommandClan extends ParentCommand {

	public CommandClan(SmartClans plugin) {
		super(plugin, null, "clan", "smartclans.command.clan", "menu");

		registerSubCommand(new SubCommandClanCreate(plugin, this));
		registerSubCommand(new SubCommandClanMenu(plugin, this));
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Only players may use this command!");
			return true;
		}

		return super.onCommand(sender, command, alias, args);
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if (!(sender instanceof Player)) {
			return Util.EMPTY_LIST;
		}

		return super.onTabComplete(sender, command, alias, args);
	}
}
