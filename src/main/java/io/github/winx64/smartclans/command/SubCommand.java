package io.github.winx64.smartclans.command;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import io.github.winx64.smartclans.SmartClans;
import io.github.winx64.smartclans.util.Util;

public abstract class SubCommand implements TabExecutor {

	protected final SmartClans plugin;
	protected final ParentCommand parent;

	protected final String name;
	protected final String permission;

	public SubCommand(SmartClans plugin, ParentCommand parent, String name, String permission) {
		this.plugin = plugin;
		this.parent = parent;

		this.name = name;
		this.permission = permission;
	}

	public final ParentCommand getParent() {
		return parent;
	}

	public final String getName() {
		return name;
	}

	public final String getPermission() {
		return permission;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		return Util.EMPTY_LIST;
	}
}
