package io.github.winx64.smartclans;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.github.winx64.smartclans.clan.Clan;

public final class ClanManager {

	private final SmartClans plugin;

	private final Map<String, Clan> clans;
	private final Map<String, Clan> pendingClans;

	public ClanManager(SmartClans plugin) {
		this.plugin = plugin;
		this.clans = new HashMap<String, Clan>();
		this.pendingClans = Collections.synchronizedMap(new HashMap<String, Clan>());
	}

	
}
