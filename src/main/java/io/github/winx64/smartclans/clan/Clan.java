package io.github.winx64.smartclans.clan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.github.winx64.smartclans.player.ClanPlayer;

public final class Clan {

	private final int id;
	private final String name;
	private String tag;

	private final List<ClanPlayer> onlinePlayers;

	public Clan(int id, String name) {
		this.id = id;
		this.name = name;
		this.tag = null;

		this.onlinePlayers = new ArrayList<ClanPlayer>();
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public List<ClanPlayer> getOnlinePlayers() {
		return Collections.unmodifiableList(onlinePlayers);
	}
}
